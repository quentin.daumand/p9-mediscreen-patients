FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /usr/app
COPY build/libs/p9-mediscreen-patients-1.0-SNAPSHOT.jar mediscreen-patients.jar
EXPOSE 8081
CMD ["java", "-jar", "mediscreen-patients.jar"]