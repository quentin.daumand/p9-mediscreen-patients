-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mediscreen
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mediscreen` ;

-- -----------------------------------------------------
-- Schema mediscreen
-- -----------------------------------------------------
CREATE SCHEMA `mediscreen` DEFAULT CHARACTER SET utf8 ;
USE `mediscreen` ;

-- -----------------------------------------------------
-- Table `mediscreen`.`patients`
-- -----------------------------------------------------
INSERT INTO `patients` VALUES (2,'745 West Valley Farms Drive','1952-09-27',NULL,'Rees','Pippa','628-423-0993','F');


CREATE TABLE IF NOT EXISTS `mediscreen`.`patients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `given_name` VARCHAR(125) NOT NULL,
  `family_name` VARCHAR(125) NOT NULL,
  `birth_date` DATE NOT NULL,
  `sex` VARCHAR(25) NOT NULL,
  `email_address` VARCHAR(125),
  `phone` VARCHAR(125),
  `address` VARCHAR(125)
  PRIMARY KEY (`id`)
);

INSERT INTO `patients` VALUES (1,'2 Warren Street','1968-06-22',NULL,'Ferguson','Lucas','387-866-1399','M');
INSERT INTO `patients` VALUES (2,'745 West Valley Farms Drive','1952-09-27',NULL,'Rees','Pippa','628-423-0993','F');
INSERT INTO `patients` VALUES (3,'599 East Garden Ave','1952-11-11',NULL,'Arnold','Edward','123-727-2779','M');
INSERT INTO `patients` VALUES (4,'894 Hall Street','1946-11-26',NULL,'Sharp','Anthony','451-761-8383','M');
INSERT INTO `patients` VALUES (5,'4 Southampton Road ','1958-06-29',NULL,'Ince','Wendy','802-911-9975','F');
INSERT INTO `patients` VALUES (6,'40 Sulphur Springs Dr','1949-12-07',NULL,'Ross','Tracey','131-396-5049','F');
INSERT INTO `patients` VALUES (7,'12 Cobblestone St','1966-12-31',NULL,'Wilson','Claire','300-452-1091','F');
INSERT INTO `patients` VALUES (8,'193 Vale St','1945-06-24',NULL,'Buckland ','Max','833-534-0864','M');
INSERT INTO `patients` VALUES (9,'12 Beechwood Road','1964-06-18',NULL,'Clark','Natalie','241-467-9197','F');
INSERT INTO `patients` VALUES (10,'1202 Bumble Dr ','1959-06-28',NULL,'Bailey','Piers','747-815-0557','M');
