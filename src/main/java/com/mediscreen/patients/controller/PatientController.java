package com.mediscreen.patients.controller;

import com.mediscreen.patients.model.PatientModel;
import com.mediscreen.patients.model.PatientModelDTO;
import com.mediscreen.patients.services.PatientService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;


@Controller
public class PatientController {
    private Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    PatientService patientService;

    @GetMapping("/")
    public String index() {
        LOGGER.info("GET /");
        return "home";
    }

    @GetMapping("/patients")
    public String patientsList(Model model) {
        LOGGER.info("GET /patients");
        model.addAttribute("patients", patientService.getAllPatients());
        return "patients";
    }

    @GetMapping("/patients/json")
    public @ResponseBody
    List<PatientModelDTO> patientListJSON() {
        LOGGER.info("GET /patients/json : OK");
        List<PatientModel> patientModList = patientService.getAllPatients();
        List<PatientModelDTO> patientModelDTOList = patientService.patientModelToDtoList(patientModList);

        return patientModelDTOList;
    }

    @GetMapping("/patient/json")
    public @ResponseBody
    PatientModelDTO getPatientJSON(Integer id) {
        LOGGER.info("GET /patient/json : OK");
        PatientModel patient = patientService.getPatientById(id);
        PatientModelDTO patientModelDTO = patientService.patientModelDTO(patient);
        return patientModelDTO;
    }

    @GetMapping("/patient/family/json")
    public @ResponseBody
    PatientModelDTO getPatientJSONFamily(String family) {
        LOGGER.info("GET /patient/json : OK");
        PatientModel patient = patientService.getPatientByFamily(family);
        PatientModelDTO patientModelDTO = patientService.patientModelDTO(patient);
        return patientModelDTO;
    }

    /**
     * HTTP GET request to get the patient add view with the attribute patientToCreate
     *
     * @param model Model Interface, to add attributes to it
     * @return string to the address "patient/add", returning the associated view
     * with attribute
     */
    @GetMapping("/patient/add")
    public String addPatients(Model model) {
        LOGGER.info("GET /patient/add");
        model.addAttribute("patientToCreate", new PatientModel());
        return "add";
    }

    /**
     * HTML POST request to add a new patient if it doesn't exist
     * Add redirect attributes messages: errorSaveMessage if the patient already exist
     *                                   successSaveMessage if the patient is new
     * Check if a Patient Already Exist.
     * -> if true, then redirect to patient/add with ErrorPatientExistentMessage
     * -> if false, then check if patients at that address already exist
     *      -> if true, then get the view  patient/add/confirmation with patients already at that address
     *      -> if false, then save patient and redirect to patient/list view
     *
     * @param patientToCreate the PatientModel with annotation @Valid (for the possible constraints)
     * @param result to represent binding results
     * @return a string to the address "patient/add" or "patient/list" or "patient/add/confirmation",
     * returning the associated view, with attributes
     */

    @PostMapping("/patient/add")
    public String patientAddValidate(@Valid @ModelAttribute("patientToCreate") PatientModel patientToCreate,
                                     BindingResult result, RedirectAttributes redirectAttributes) {
        LOGGER.info("POST /patient/add");
        if (!result.hasErrors()) {
            if (patientService.checkGivenAndFamilyAndDobExist(patientToCreate.getGiven(),
                    patientToCreate.getFamily(), patientToCreate.getDob())) {
                LOGGER.info("/patient/add : Patient already exist");
                redirectAttributes.addFlashAttribute("ErrorPatientExistentMessage", "Patient already exist");
                return "redirect:/patient/add";
            }
            else {
                    patientService.save(patientToCreate);
                    LOGGER.info("POST /patient/add : OK");
                    redirectAttributes.addFlashAttribute("successSaveMessage", "Patient was successfully added");
                    return "redirect:/patients";
                }
            }
        return "add";
    }


    @GetMapping("/patient/update/{id}")
    public String patientUpdate(@PathVariable("id") Integer id, Model model, RedirectAttributes ra) {
        if (patientService.checkIdExists(id) == false) {
            ra.addFlashAttribute("ErrorPatientIdMessage", "Patient ID doesn't exist");
            LOGGER.info("GET /patient/update : Non existent id");
            return "redirect:/patients";
        }

        model.addAttribute("patient", patientService.getPatientById(id));
        LOGGER.info("GET /patient/update : OK");
        return "update";
    }

    @PostMapping("/patient/update/{id}")
    public String patientPostUpdate(@PathVariable("id") Integer id,
                                    @Valid @ModelAttribute("patient") PatientModel patient,
                                    BindingResult result, RedirectAttributes ra) {
        if (!patientService.checkIdExists(id)) {
            ra.addFlashAttribute("ErrorPatientIdMessage", "Patient ID doesn't exist");
            LOGGER.info("GET /patient/update : Non existent id");
            return "redirect:/patients";
        }
        if (!result.hasErrors()) {
            patientService.update(patient);
            ra.addFlashAttribute("successUpdateMessage", "Your patient was successfully updated");
            LOGGER.info("POST /patient/update : OK");
            return "redirect:/patients";
        }
        LOGGER.info("POST /patient/update : ERROR");
        return "patient/update";
    }

    @GetMapping("/patient/delete/{id}")
    public String patientDelete(@PathVariable("id") Integer id, Model model, RedirectAttributes ra) {
        try {
            if (patientService.checkIdExists(id) == false) {
                ra.addFlashAttribute("ErrorPatientIdMessage", "Patient ID doesn't exist");
                LOGGER.info("GET /patient/delete : Non existent patient id");
                return "redirect:/patients";
            }
            patientService.delete(id);
            model.addAttribute("patient", patientService.getAllPatients());
            ra.addFlashAttribute("successDeleteMessage", "This patient was successfully deleted");
            LOGGER.info("/patient/delete : OK");

        } catch (Exception e) {
            ra.addFlashAttribute("errorDeleteMessage", "Error during deletion of the patient");
            LOGGER.info("/patient/delete : KO " + "Invalid patient ID " + id);
        }
        return "redirect:/patients";
    }

    @GetMapping("/check/patient/{id}")
    public @ResponseBody boolean checkPatientId(@PathVariable("id") Integer patientId) {
        LOGGER.info("GET /check/patient : OK");
        return patientService.checkIdExists(patientId);
    }

    @GetMapping("/check/familyPatient/{family}")
    public @ResponseBody boolean checkPatientFamily(@PathVariable("family") String family) {
        LOGGER.info("GET /check/patient : OK");
        return patientService.checkFamilyExists(family);
    }


}
