package com.mediscreen.patients.model;

import com.mediscreen.patients.repository.SexENUM;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class PatientModelDTO {
    private Integer id;

    private String given;

    private String family;

    private String dob;

    @Enumerated(EnumType.STRING)
    private SexENUM sex;

    private String address;

    private String emailAddress;

    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setGiven(String given) {
        this.given = given;
    }

    public String getGiven() {
        return this.given;
    }

    public String getFamily() {
        return family;
    }

    public String getDob() {
        return dob;
    }

    public SexENUM getSex() {
        return sex;
    }

    public String getAddress() {
        return address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setSex(SexENUM sex) {
        this.sex = sex;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
