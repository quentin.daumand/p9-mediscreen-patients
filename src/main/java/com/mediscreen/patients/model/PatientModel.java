package com.mediscreen.patients.model;

import com.mediscreen.patients.repository.SexENUM;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

@Entity
@Table(name = "patients")
public class PatientModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "given_name")
    @NotEmpty(message="Given name cannot be empty")
    @Size(min=2, max=25, message="Given name must be between 2 and 125 characters")
    private String given;

    @Column(name = "family_name")
    @NotEmpty(message="Family name cannot be empty")
    @Size(min=2, max=25, message="Family name must be between 2 and 125 characters")
    private String family;

    @Past(message = "The date should be a date in the future or now")
    @NotNull(message="Dob cannot be empty")
    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dob;

    @Enumerated(EnumType.STRING)
    @Column()
    private SexENUM sex;

    @Column(name = "address")
    private String address;

    @Column(name = "email_address")
    @Email
    private String emailAddress;

    @Column(name = "phone")
    @Size(max = 30)
    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGiven() {
        return given;
    }

    public void setGiven(String given) {
        this.given = given;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public SexENUM getSex() {
        return sex;
    }

    public void setSex(SexENUM sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}