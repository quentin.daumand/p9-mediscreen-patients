package com.mediscreen.patients.services;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.mediscreen.patients.model.PatientModel;
import com.mediscreen.patients.model.PatientModelDTO;
import com.mediscreen.patients.repository.PatientRepository;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {
    private Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    private PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientModel> getAllPatients() {
        return patientRepository.findAll();
    }

    /**
     * Set a new AddressModel from data inside the request, to the PatientModel
     * Save a new patient in the DB after checking if the given, family names and birthdate exist already
     *
     * @param patient the PatientModel to save
     * @return patient saved
     */
    public PatientModel save(PatientModel patient) {
        patientRepository.save(patient);
        return patient;
    }


    public PatientModel update(PatientModel patient) {
        patientRepository.save(patient);
        return patient;
    }


    /**
     * Delete an existent patient from the DB
     * @param id the integer of patient ID
     */
    public void delete(int id) {
        patientRepository.deleteById(id);
    }

    public boolean checkGivenAndFamilyAndDobExist(String given, String family, LocalDate dob) {
        return patientRepository.existsByGivenAndFamilyAndDob(given, family, dob);
    }

    public List<PatientModel> getAllPatientsByAddress(String street) {
        return patientRepository.findAllByAddress(street);
    }

    public boolean checkIdExists(int id) {
        return patientRepository.existsById(id);
    }

    public boolean checkFamilyExists(String family) {
        return patientRepository.existsByFamily(family);
    }

    public PatientModel getPatientById(int id) {
        return patientRepository.findById(id);
    }


    /**
     * Convert a PatientModel list into a PatientModelDTO list
     *
     * @param patientModelList a list of patients in PatientModel
     * @return a list of PatientModelDTO
     */
    public List<PatientModelDTO> patientModelToDtoList(List<PatientModel> patientModelList) {
        List<PatientModelDTO> patientModelDTOList = new ArrayList<>();
        for (PatientModel patientModel: patientModelList) {
            PatientModelDTO patientModelDTO = new PatientModelDTO();
            patientModelDTO.setId(patientModel.getId());
            patientModelDTO.setGiven(patientModel.getGiven());
            patientModelDTO.setFamily(patientModel.getFamily());
            patientModelDTO.setAddress(patientModel.getAddress());
            patientModelDTO.setDob(patientModel.getDob().toString());
            patientModelDTO.setSex(patientModel.getSex());
            patientModelDTO.setEmailAddress(patientModel.getEmailAddress());
            patientModelDTO.setPhone(patientModel.getPhone());
            patientModelDTOList.add(patientModelDTO);
        }

        return patientModelDTOList;
    }

    public PatientModelDTO patientModelDTO(PatientModel patientModel) {
        PatientModelDTO patientModelDTO = new PatientModelDTO();

        patientModelDTO.setId(patientModel.getId());
        patientModelDTO.setGiven(patientModel.getGiven());
        patientModelDTO.setFamily(patientModel.getFamily());
        patientModelDTO.setAddress(patientModel.getAddress());
        patientModelDTO.setDob(patientModel.getDob().toString());
        patientModelDTO.setSex(patientModel.getSex());
        patientModelDTO.setEmailAddress(patientModel.getEmailAddress());
        patientModelDTO.setPhone(patientModel.getPhone());

        return patientModelDTO;
    }

    public PatientModel getPatientByFamily(String family) {
        return patientRepository.findByFamily(family);
    }
}
