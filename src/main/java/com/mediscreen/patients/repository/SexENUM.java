package com.mediscreen.patients.repository;

public enum SexENUM {
    M("MALE"),
    F("FEMALE");

    private final String displayValue;

    private SexENUM(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}