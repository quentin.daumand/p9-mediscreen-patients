package com.mediscreen.patients.repository;

import com.mediscreen.patients.model.PatientModel;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<PatientModel, Integer> {
    List<PatientModel> findAll();

    boolean existsByGivenAndFamilyAndDob(String given, String family, LocalDate dob);

    List<PatientModel> findAllByAddress(String street);

    PatientModel findById(int id);

    boolean existsByFamily(String family);

    PatientModel findByFamily(String family);
}
