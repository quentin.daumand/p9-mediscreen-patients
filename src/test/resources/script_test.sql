-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mediscreen_test
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mediscreen_test` ;

-- -----------------------------------------------------
-- Schema mediscreen
-- -----------------------------------------------------
CREATE SCHEMA `mediscreen_test` DEFAULT CHARACTER SET utf8 ;
USE `mediscreen_test` ;

-- -----------------------------------------------------
-- Table `mediscreen_test`.`patients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mediscreen_test`.`patients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `given_name` VARCHAR(125) NOT NULL,
  `family_name` VARCHAR(125) NOT NULL,
  `birth_date` DATE NOT NULL,
  `sex` VARCHAR(25) NOT NULL,
  `email_address` VARCHAR(125),
  `phone` VARCHAR(125),
  `address` VARCHAR(125),
  PRIMARY KEY (`id`)
);

