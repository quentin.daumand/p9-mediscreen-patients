package com.mediscreen.patients.services;

import com.mediscreen.patients.model.PatientModel;
import com.mediscreen.patients.model.PatientModelDTO;
import com.mediscreen.patients.repository.SexENUM;
import com.mediscreen.patients.repository.PatientRepository;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PatientServiceTest {

    @Autowired
    private PatientService patientService;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PatientRepository patientRepository;

    public PatientModel Sophie() {
        LocalDate date = new LocalDate(1994, 07, 10);
        PatientModel Sophie = new PatientModel();
        Sophie.setGiven("Sophie");
        Sophie.setFamily("Princess");
        Sophie.setDob(date);
        Sophie.setSex(SexENUM.F);
        Sophie.setAddress("1 rue du Paradis");
        Sophie.setEmailAddress("princessSophie@mail.fr");
        Sophie.setPhone("0102030405");
        return Sophie;
    }

    public PatientModel Luc() {
        LocalDate date = new LocalDate(1984, 9,04);
        PatientModel patientModel2 = new PatientModel();
        patientModel2.setGiven("Luc");
        patientModel2.setFamily("Besson");
        patientModel2.setDob(date);
        patientModel2.setSex(SexENUM.M);
        patientModel2.setAddress("666 rue de l'Enfer");
        patientModel2.setEmailAddress("lub.besson@gmail.com");
        patientModel2.setPhone("001122334455");
        return patientModel2;
    }

    @Before
    public void savePatientsToDbBeforeTests() throws SQLException {
        ScriptUtils.executeSqlScript(dataSource.getConnection(), new FileSystemResource("src/test/resources/script_test.sql"));
        patientRepository.deleteAll();
        patientService.save(Sophie());
        patientService.save(Luc());
    }

    @After
    public void deleteAllPatientsAfterTests() {
        patientRepository.deleteAll();
    }

    @Test
    public void getAllPatientsReturnAllPatients() {
        List<PatientModel> listPatient = patientService.getAllPatients();
        Assert.assertTrue(listPatient.size() == 2);
        Assert.assertTrue(listPatient.get(0).getGiven().equals("Sophie"));
        Assert.assertTrue(listPatient.get(0).getAddress().equals("1 rue du Paradis"));
    }

    @Test
    public void checkExistentPatientByFamilysDobOK() {
        String given = "Luc";
        String family = "Besson";
        LocalDate date = new LocalDate(1984, 9,04);
        boolean existentPatient = patientService.checkGivenAndFamilyAndDobExist(given, family,
                date);
        Assert.assertEquals(true, existentPatient);
    }

    //All Patients By Address Should Return Patients At That Address
    @Test
    public void getAllPatientsByAddressOK() {
        String street = "1 rue du Paradis";

        List<PatientModel> listPatient =
                patientService.getAllPatientsByAddress(street);

        Assert.assertTrue(listPatient.size() == 1);
        Assert.assertTrue(listPatient.get(0).getGiven().equals("Sophie"));
        Assert.assertTrue(listPatient.get(0).getAddress().equals("1 rue du Paradis"));
    }

    //checkExistentPatientByIdShouldReturnTrue
    @Test
    public void checkExistentPatientByIdOK() {
        int id = 1;

        boolean existentPatientById = patientService.checkIdExists(id);
        Assert.assertEquals(true, existentPatientById);
    }

    //Patient By Id Should Return Patient With That Id
    @Test
    public void getPatientByIdOk() {
        int id = 1;

        PatientModel patient = patientService.getPatientById(id);

        Assert.assertTrue(patient.getGiven().equals("Sophie"));
        Assert.assertTrue(patient.getSex().equals(SexENUM.F));
        Assert.assertTrue(patient.getAddress().equals("1 rue du Paradis"));
    }

    @Test
    public void deletePatientByIdOK() {
        int id = 1;

        patientService.delete(id);
        Optional<PatientModel> optionalPatientModel = Optional.ofNullable(patientService.getPatientById(id));

        Assert.assertFalse(optionalPatientModel.isPresent());
    }


    @Test
    public void updatePatientOK() {
        PatientModel patientModel3 = patientService.getPatientById(1);
        patientModel3.setAddress("NewAddress");
        PatientModel patientToUpdate = patientService.update(patientModel3);

        Assert.assertEquals("NewAddress", patientToUpdate.getAddress());
    }

    @Test
    public void patientModelToDtoList() {
        List<PatientModel> myList = new ArrayList<>();
        myList.add(Sophie());
        List<PatientModelDTO> DTOSophieList = patientService.patientModelToDtoList(myList);
        Assert.assertEquals("Sophie", DTOSophieList.get(0).getGiven());
    }

    @Test
    public void patientModelToDto() {
        PatientModelDTO DTOSophie = patientService.patientModelDTO(Sophie());
        Assert.assertEquals("Sophie", DTOSophie.getGiven());
    }

}
