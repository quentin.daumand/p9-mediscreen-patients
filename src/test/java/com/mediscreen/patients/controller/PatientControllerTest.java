package com.mediscreen.patients.controller;

import com.mediscreen.patients.model.PatientModel;
import com.mediscreen.patients.repository.SexENUM;
import com.mediscreen.patients.services.PatientService;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class PatientControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webContext;

    @MockBean
    PatientService patientService;

    @Before
    public void setupMockmvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();
    }

    public PatientModel Sophie() {
        LocalDate date = new LocalDate(1994, 07, 10);
        PatientModel Sophie = new PatientModel();
        Sophie.setGiven("Sophie");
        Sophie.setFamily("Princess");
        Sophie.setDob(date);
        Sophie.setSex(SexENUM.F);
        Sophie.setAddress("1 rue du Paradis");
        Sophie.setEmailAddress("princessSophie@mail.fr");
        Sophie.setPhone("0102030405");
        return Sophie;
    }

    public PatientModel Luc() {
        LocalDate date = new LocalDate(1984, 9,04);
        PatientModel patientModel2 = new PatientModel();
        patientModel2.setGiven("Luc");
        patientModel2.setFamily("Besson");
        patientModel2.setDob(date);
        patientModel2.setSex(SexENUM.M);
        patientModel2.setAddress("666 Rue de L'enfer");
        patientModel2.setEmailAddress("lub.besson@gmail.com");
        patientModel2.setPhone("001122334455");
        return patientModel2;
    }

    @Test
    public void getHome() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("home"));
    }

    @Test
    public void patientListJSON() throws Exception {
        mockMvc.perform(get("/patients/json"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void patientJSON() throws Exception {
        mockMvc.perform(get("/patient/json")
                        .param("id", "1"))
                .andExpect(status().is2xxSuccessful());
    }


    //Request PatientListView Should Return Success
    @Test
    public void getRequestPatientListOk() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(patientList)
                .when(patientService)
                .getAllPatients();

        mockMvc.perform(get("/patients"))

                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("patients"))
                .andExpect(model().attributeExists("patients"))
                .andReturn();
        assertTrue(patientList.get(0).getGiven().equals("Sophie"));
    }

    //Request PatientAddView Should Return Success
    @Test
    public void getRequestPatientAddViewOk() throws Exception {
        mockMvc.perform(get("/patient/add"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("add"))
                .andExpect(model().attributeExists("patientToCreate"))
                .andReturn();
    }
//Request PatientAddValidate Given Good Parameters Should Return ErrorPatientExistentMessage
    @Test
    public void postRequestPatientAddValidateAlreadyExistent() throws Exception {
        doReturn(true)
                .when(patientService)
                .checkGivenAndFamilyAndDobExist(Sophie().getGiven(),
                        Sophie().getFamily(), Sophie().getDob());

        MvcResult mvcResult = mockMvc.perform(post("/patient/add")
                        .flashAttr("ErrorPatientExistentMessage", "Patient already exist")
                        .param("id", "1")
                        .param("given", "Sophie")
                        .param("family", "Princess")
                        .param("dob", "1994-07-10")
                        .param("sex", "F")
                        .param("address", "1 rue du Paradis")
                        .param("emailAddress", "princessSophie@gmail.com")
                        .param("phone", "0102030405"))

                .andExpect(model().hasNoErrors())
                .andExpect(view().name("redirect:/patient/add"))
                .andExpect(flash().attributeExists("ErrorPatientExistentMessage"))
                .andReturn();
    }
//Request PatientAddValidate Given Good Parameters Should Return PatientAddConfirmationView
    @Test
    public void postRequestPatientAddValidateConfirm() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(false)
                .when(patientService)
                .checkGivenAndFamilyAndDobExist(Sophie().getGiven(),
                        Sophie().getFamily(), Sophie().getDob());

        mockMvc.perform(post("/patient/add")
                        .flashAttr("patientToCreate", Sophie())
                        .param("id", "1")
                        .param("given", "Sophie")
                        .param("family", "Princess")
                        .param("dob", "1994-07-10")
                        .param("sex", "F")
                        .param("address", "1 rue du Paradis")
                        .param("emailAddress", "princessSophie@mail.fr")
                        .param("phone", "0102030405"))

                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andReturn();
    }
//Request PatientAddValidate Given Good Parameters Should SavePatient And Return PatientListView
    @Test
    public void postRequestPatientAddValidateSaveAndView() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(Sophie())
                .when(patientService)
                .save(Sophie());

        mockMvc.perform(post("/patient/add")
                        .param("id", "1")
                        .param("given", "Sophie")
                        .param("family", "Princess")
                        .param("dob", "2020-01-01")
                        .param("sex", "M")
                        .param("address", "StreetTest1")
                        .param("emailAddress", "EmailTest1@email.com")
                        .param("phone", "004678925899"))

                .andExpect(view().name("redirect:/patients"))
                .andReturn();
    }

//Request PatientUpdate With InvalidId Should Redirect To PatientList With ErrorPatientId Message
    @Test(expected = Exception.class)
    public void getRequestPatientUpdateKO() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doThrow(Exception.class)
                .when(patientService)
                .checkIdExists(1);

        mockMvc.perform(get("/patient/update/{id}", "5")
                        .flashAttr("InvalidPatientIdMessage", "Invalid patient ID"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("InvalidPatientIdMessage"))
                .andReturn();
    }

    //Request Patient Update With Existent Id Should Redirect PatientUpdateView
    @Test
    public void getRequestPatientUpdateKORedirect() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(true)
                .when(patientService)
                .checkIdExists(1);

        doReturn(Sophie())
                .when(patientService)
                .getPatientById(1);

        mockMvc.perform(get("/patient/update/{id}", "1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("update"))
                .andExpect(model().attributeExists("patient"))
                .andReturn();

        assertTrue(Sophie().getFamily().equals("Princess"));
    }
    //Request PatientUpdate With NonExistentId Should Redirect To PatientList With Error PatientIdMessage
    @Test
    public void getRequestPatientUpdateKo() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(false)
                .when(patientService)
                .checkIdExists(1);

        mockMvc.perform(get("/patient/update/{id}", "1")
                        .flashAttr("ErrorPatientIdMessage", "Patient ID doesn't exist"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("ErrorPatientIdMessage"))
                .andReturn();
    }

    @Test
    public void postRequestPatientPostUpdateKO() throws Exception {
        doReturn(false)
                .when(patientService)
                .checkIdExists(1);

        mockMvc.perform(post("/patient/update/{id}", "18")
                        .flashAttr("ErrorPatientIdMessage", "Patient ID doesn't exist")
                        .param("id", "1")
                        .param("given", "Sophie")
                        .param("family", "Boyd")
                        .param("dob", "2020-01-01")
                        .param("sex", "M")
                        .param("address.street", "StreetTest1")
                        .param("address.city", "CityTest1")
                        .param("address.postcode", "112345")
                        .param("address.district", "DistrictTest1")
                        .param("address.state", "StateTest1")
                        .param("address.country","CountryTest1")
                        .param("emailAddress", "EmailTest1@email.com")
                        .param("phone", "004678925899"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("ErrorPatientIdMessage"))
                .andReturn();
    }

    @Test
    public void postRequestPatientPostUpdateOk() throws Exception {
        doReturn(true)
                .when(patientService)
                .checkIdExists(1);

        doReturn(Sophie())
                .when(patientService)
                .update(Sophie());

        mockMvc.perform(post("/patient/update/{id}", "1")
                        .flashAttr("successUpdateMessage", "Your patient was successfully updated")
                        .param("id", "1")
                        .param("given", "Sophie")
                        .param("family", "Princess")
                        .param("dob", "2020-01-01")
                        .param("sex", "M")
                        .param("address", "StreetTest1")
                        .param("emailAddress", "EmailTest1@email.com")
                        .param("phone", "004678925899"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("successUpdateMessage"))
                .andReturn();
    }
    //Request PatientPostUpdate WithInvalidId Should Return PatientAddView
    @Test(expected = Exception.class)
        public void postRequestPatientPostUpdateKoReturnPatientAddView() throws Exception {
        doReturn(true)
                .when(patientService)
                .checkIdExists(1);

        doThrow(Exception.class)
                .when(patientService)
                .update(Sophie());

        mockMvc.perform(post("/patient/update/{id}", "#{|#@#{[[["))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("redirect:/patients"))
                .andReturn();
    }
    //Request Patient DeleteId Should Return Success
    @Test
    public void getRequestPatientDeleteOk() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(true)
                .when(patientService)
                .checkIdExists(1);

        doNothing()
                .when(patientService)
                .delete(1);

        doReturn(patientList)
                .when(patientService)
                .getAllPatients();
        mockMvc.perform(get("/patient/delete/{id}", "1")
                        .flashAttr("successDeleteMessage", "This patient was successfully deleted"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("successDeleteMessage"))
                .andReturn();
        assertTrue(Sophie().getFamily().equals("Princess"));
    }

    //RequestPatientDeleteWithNonExistentIdShouldRedirectToPatientList
    @Test
    public void getRequestPatientDeleteKoRedirectToPatientList() throws Exception {
        List<PatientModel> patientList = new ArrayList<>();
        patientList.add(Sophie());

        doReturn(false)
                .when(patientService)
                .checkIdExists(1);
        mockMvc.perform(get("/patient/delete/{id}", "25")
                        .flashAttr("ErrorPatientIdMessage", "Patient ID doesn't exist"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("ErrorPatientIdMessage"))
                .andReturn();
    }
    //Request Patient Delete With WrongId Should Redirect To PatientList
    @Test(expected = Exception.class)
    public void getRequestPatientDeleteKoIDRedirectToPatientList() throws Exception {
        doThrow(Exception.class)
                .when(patientService)
                .checkIdExists(1);

        mockMvc.perform(get("/patient/delete/{id}", "#{@[@[")
                        .flashAttr("errorDeleteMessage", "Error during deletion of the patient"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/patients"))
                .andExpect(flash().attributeExists("errorDeleteMessage"))
                .andReturn();
    }

}