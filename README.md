#Readme
## P9 MEDISCREEN PATIENTS
### Purpose
 Mediscreen specializes in detecting risk factors for disease. Our screenings are using predictive analysis of patient populations at an affordable cost
This application is designed to:
<ul>
    <li>to handle a MySQL Database</li>
    <li>handle patient's records in a Mongo DB</li>
    <li>provide a screening of all patients according to specific criteria (see ENUM classes)</li>
</ul>


### Prerequisites to run
<ul>
<li>Java JDK11</li>
<li>Gradle 4.8.1</li>
<li>Docker</li>
<li>MongoDB Compass & an account</li>
<li>MySQL 8.0</li>
</ul>

### Installing
<p>
<ol>
    <li>Install <strong>Java</strong>: https://www.oracle.com/java/technologies/javase-downloads.html</li>
    <li>Install <strong>Gradle</strong>: https://gradle.org/install/</li>
    <li>Install <strong>Docker Desktop</strong>: https://docs.docker.com/docker-for-windows/</li>
    <li>Install <strong>MySQL</strong>: https://dev.mysql.com/downloads/mysql/</li>
    <li>Install <strong>MongoDB Compass</strong>: https://www.mongodb.com/products/compass</li>
</ol>


### Technical Specifications

Mediscreen is composed of multiple microservices:
<ol>
    <li>Patients: https://gitlab.com/quentin.daumand/p9-mediscreen-patients</li>
    <li>Notes: https://gitlab.com/quentin.daumand/p9-mediscreen-notes</li>
    <li>Risk: https://gitlab.com/quentin.daumand/p9-mediscreen-risk</li>
</ol>


### Run the application

<strong>WITH IDE/GRADLE</strong>: Use the BASE_URL_LOCALHOST constant present in files PatientWebClientService/RecordWebClientService in microservice REPORTS :src/main/java/com/mediscreen/reports/service/webclient and in
in microservice RECORDS :src/main/java/com/mediscreen/records/service/

For running the application, either launch it in your IDE or run below command inside the root directory of the 3 microservices.
You will also need to change the mongoDB URL in application.properties of microservice RECORDS, to work with your cluster.
```
$ ./gradlew bootRun
```

<strong>WITH DOCKER</strong>Use this exact procedure:
<ol>
    <li>Build your application with gradle ```$ ./gradlew build```</li>
    <li>Build docker images for each microservice. In each microservice root directory, launch the following command:```$ docker build -t NAME_OF_YOUR_IMAGE:TAGVERSION .``````</li>
    <li>Go into root directory of microservice REPORTS and launch this command, which is going to launch and compose the Docker containers from images previously created, but also run them.
        You will then deploy with Docker all the applications running with Spring Boot.</li>
</ol>

<strong>Tips</strong>Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application's services.

Then, with a single command, you create and start all the services from your configuration. ...
Run docker-compose up and Compose starts and runs your entire app.

```$ docker-compose up```


### Endpoints
```
> **GET** - README
http://localhost:8081/

> **GET** - Patient List of all patients
http://localhost:8081/patients

> **GET** - JSON of all patients
http://localhost:8081/patients/json

> **GET** - JSON of patient with id
http://localhost:8081/patient/json
**OBLIGATORY PARAMETERS**: id=?

> **GET** - JSON of patient with family
http://localhost:8081/patient/family/json
**OBLIGATORY PARAMETERS**: family=?

> **GET** - Add a patient
http://localhost:8081/patient/add/

> **POST** - Add a patient
http://localhost:8081/patient/add/ <br>
**OBLIGATORY(param marked as * non-obligatory) parameters**: given, family, dob ("yyyy-MM-dd"), sex, address emailAddress(*), phone(*)

> **GET** - Update a patient - view
http://localhost:8081/patient/update/{id}

> **POST** - Update a patient
http://localhost:8081/patient/update/{id}

> **GET** - Delete a patient
http://localhost:8081/patient/delete/{id}

 > **GET** - Check if patient id exists
http://localhost:8081/check/patient/{id}

> **GET** - Check if patient family exists
http://localhost:8081/check/familyPatient/{family}
```

### Testing

Mediscreen has a full integration and unit test suite in each microservice. You can launch it with the following command inside the root directory:

```
    $ ./gradlew test
```

Or for running the tests inside your IDE, follow the link below:
[Work with tests in Gradle](https://www.jetbrains.com/help/idea/work-with-tests-in-gradle.html#configure_gradle_test_runner).


### Reporting

JaCoCo reporting tools are attached to the gradle configuration. Launch these 2 commands to run the tools:
```
$ ./gradlew jacocoTestReport
$ ./gradlew jacocoTestCoverageVerification
    ```
